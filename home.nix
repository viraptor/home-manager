{pkgs, bundix, nixpkgs-unstable, nixpkgs-2211, nixpkgs-2205, ...}: let
    read-secret = (name: strings.fileContents /Users/viraptor/.config/home-manager/secrets/${name});
    jitruby = (pkgs.ruby_3_1.override { jitSupport = true; } );
    pkgs-unstable = import nixpkgs-unstable {};
    pkgs-2211 = import nixpkgs-2211 {};
    pkgs-2205 = import nixpkgs-2205 {};
    envato-iamy = pkgs.buildGoModule rec {
        pname = "iamy";
        version = "5.0.1";

        src = pkgs.fetchFromGitHub {
          owner = "envato";
          repo = "iamy";
          rev = "v${version}";
          hash = "sha256-t73q72An2518YK2UPt/cMSsoh/1StInRYhXwk+XeigI=";
        };

        proxyVendor = true;
        vendorHash = "sha256-joVNR5pqkYNd9HQ4retqvj1RfdkluSdsfrSVnMtLans=";

        ldflags = [
          "-X main.Version=v${version}+envato" "-s" "-w"
        ];

        meta = with pkgs.lib; {
          description = "A cli tool for importing and exporting AWS IAM configuration to YAML files";
          homepage = "https://github.com/envato/iamy";
          license = licenses.mit;
          maintainers = with maintainers; [ ];
        };
      };
    ejsonkms = pkgs.buildGoModule rec {
        pname = "ejsonkms";
        version = "0.2.0";

        src = pkgs.fetchFromGitHub {
          owner = "envato";
          repo = "ejsonkms";
          rev = "v${version}";
          hash = "sha256-cjL2MeQhRvWIDfBiGK8bhxLjwSh5dJtOUvnqybOlyZE=";
        };

        vendorHash = "sha256-1N2FzDZi7PfCdYr19MeR/La5hDdQlRf8hla6MmYn/I8=";

        ldflags = [
          "-X main.version=v${version}" "-s" "-w"
        ];

        doCheck = false;

        meta = with pkgs.lib; {
          description = "Integrates EJSON with AWS KMS ";
          homepage = "https://github.com/envato/ejsonkms";
          license = licenses.mit;
          maintainers = with maintainers; [ ];
        };
      };
    bk = pkgs.buildGoModule rec {
        pname = "bk";
        version = "2.0.0";

        src = pkgs.fetchFromGitHub {
          owner = "buildkite";
          repo = "cli";
          rev = "v${version}";
          hash = "sha256-4MUgyUKyycsreAMVtyKJFpQOHvI6JJSn7TUZtbQANyc=";
        };

        vendorHash = "sha256-3x7yJenJ2BHdqVPaBaqfFVeOSJZ/VRNF/TTfSsw+2os=";

        ldflags = [
          "-s" "-w"
        ];

        meta = with pkgs.lib; {
          description = "A command line interface for Buildkite";
          homepage = "https://github.com/buildkite/cli";
          license = licenses.mit;
          maintainers = with maintainers; [ ];
        };
      };
    strings = pkgs.lib.strings;
  in {
  #nixpkgs.overlays = [ my-flakes.overlay ];
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [
    "openssl-1.1.1u"
  ];

  home.homeDirectory = "/Users/viraptor";
  home.username = "viraptor";
  home.stateVersion = "23.05";

  programs.home-manager.enable = true;

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    plugins = [
      {
        name = "zsh-nix-shell";
        file = "nix-shell.plugin.zsh";
        src = pkgs.fetchFromGitHub {
          owner = "chisui";
          repo = "zsh-nix-shell";
          rev = "v0.5.0";
          sha256 = "0za4aiwwrlawnia4f29msk822rj9bgcygw6a8a6iikiwzjjz0g91";
        };
      }
      #{
      #  name = "zsh-fzf-history-search";
      #  file = "zsh-fzf-history-search.plugin.zsh";
      #  src = pkgs.fetchFromGitHub {
      #    owner = "joshskidmore";
      #    repo = "zsh-fzf-history-search";
      #    rev = "d1aae98ccd6ce153c97a5401d79fd36418cd2958";
      #    hash = "sha256-4Dp2ehZLO83NhdBOKV0BhYFIvieaZPqiZZZtxsXWRaQ=";
      #  };
      #}
    ];
    oh-my-zsh = {
      enable = true;
      plugins = [ "git" "python" "man" ];
      theme = "robbyrussell";
    };
    loginExtra = ''
      bindkey "[D" backward-word
      bindkey "[C" forward-word
      bindkey "^[a" beginning-of-line
      bindkey "^[e" end-of-line
      ssh-agent -a ~/.ssh/agent-sock -s -P '/nix/store/*-yubico-piv-tool-*/lib/*.dylib'
      ${pkgs.gnupg}/bin/gpgconf --launch gpg-agent

      autoload bashcompinit && bashcompinit
      complete -C '${pkgs.awscli2}/bin/aws_completer' aws

      source ${./_aws-sso}
      compdef _aws-sso aws-sso

      source ${pkgs.fzf}/share/fzf/key-bindings.zsh


      if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
        . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
      fi

      if [ -z "$IN_NIX_SHELL" ] ; then
        eval $(/Users/viraptor/.nix-profile/bin/asdf info | grep ^ASDF_DIR)
        . "''${ASDF_DIR}/asdf.sh"
      fi
    '';
    shellAliases = {
      ase = "aws-sso exec -p";
      asc = "aws-sso console -p";
      ave = "aws-vault exec";
      avc = "aws-vault login";
    };
    envExtra = ''
      export EDITOR=vim
      export SAM_CLI_TELEMETRY=0
      export DISABLE_SPRING=1
      export AWS_PAGER="${pkgs.less}/bin/less -F"
      export LESSOPEN="|${pkgs.lesspipe}/bin/lesspipe.sh %s" LESS_ADVANCED_PREPROCESSOR=1
    '';
  };

  programs.git = {
    enable = true;
    package = (pkgs.git.override { svnSupport = false; sendEmailSupport = false; guiSupport = false; } );
    userEmail = "stan.pitucha@envato.com";
    userName = "Stanisław Pitucha";
    ignores = [ "*~" ".DS_Store" ];
    extraConfig = {
      push = {
        default = "current";
      };
      alias = {
        pr = "pull --rebase";
        co = "checkout";
        cp = "cherry-pick";
        amend = "commit --amend";
        ci = "commit";
        ri = "rebase -i";
      };
      buildkite = {
        apikey = (read-secret "buildkite/apikey");
      };
      diff = {
        #external = "${pkgs-unstable.difftastic}/bin/difft";
        #noprefix = true;
      };
      gpg = {
        program = "${pkgs.gnupg}/bin/gpg";
      };
      tig = {
        line-graphics = "utf-8";
      };
      core.excludesfile = let globalignore = pkgs.writeTextFile {
        name = "global_gitignore";
        text = ''
          /.idea/
          .*.sw[op]
          .DS_Store
        '';
      }; in
      "${globalignore}";
    };
  };

  home.packages = [
    pkgs.tig
    pkgs.gitui

    pkgs.shellcheck
    pkgs-2211.gdb
    pkgs.asdf-vm
    pkgs.bats
    pkgs-unstable.rbspy

    pkgs.ack
    pkgs.gawk
    pkgs.bash
    pkgs.direnv

    pkgs.awscli2
    pkgs-2205.awless
    pkgs.aws-vault
    envato-iamy
    pkgs.buildkite-cli

    pkgs.cf-vault
    pkgs.cloudflared
    pkgs.python39Packages.cfn-lint
    pkgs.ejson2env
    ejsonkms

    pkgs.btop
    pkgs.curl
    pkgs.fd
    pkgs.fzf
    pkgs.gnupg
    pkgs.jq
    pkgs.ijq
    pkgs.yq
    pkgs-unstable.fq
    pkgs.lesspipe
    pkgs.less
    pkgs.gnumake
    pkgs.ncdu_1
    pkgs.duf
    pkgs-unstable.doggo
    pkgs.pstree
    pkgs.python39Packages.ipython
    pkgs.unzip
    pkgs.vim
    pkgs.wget
    pkgs.xz
    pkgs.zstd

    pkgs.graphviz
    pkgs.imagemagick

    pkgs.ffmpeg
    pkgs.caddy

    pkgs.miniupnpc
    pkgs.wireshark

    jitruby
    (import bundix { inherit pkgs; ruby = jitruby; })
    pkgs.go
    # pkgs.crystal
    pkgs.gemstash

    pkgs._1password
    pkgs.yubikey-manager
    pkgs.libyubikey
    pkgs-2205.yubico-piv-tool
    pkgs.pinentry_mac
    pkgs.pinentry
    pkgs.gpgme

    #pkgs.openssh_hpn
    pkgs.openssh
    pkgs.ssh-copy-id

    pkgs.colima
    pkgs.docker-client
    pkgs.docker-compose
    #pkgs.afsctool

    pkgs.mycli

    pkgs.cmake
    pkgs.pprof

    pkgs.heroku
    pkgs.discord

    pkgs.audiowaveform
    pkgs.libsodium
    pkgs.ssm-session-manager-plugin
    pkgs.aws-sam-cli
    pkgs-unstable.aws-sso-cli
    pkgs.libyaml
    #pkgs.elasticsearch7
    pkgs.jre_headless

    #pkgs.datadog-agent

    # pkgs.iterm2 - install tools

    #devenv.packages.x86_64-darwin.devenv
  ];

  home.file = {
    asdfrc = { target = ".asdfrc"; text = "legacy_version_file = yes\n"; };
    bundle = {
      target = ".bundle/config";
      text = ''
        ---
        BUNDLE_RUBYGEMS__ENVATO__NET: "marketplace_dev_2017:${read-secret "rubygems-envato.key"}"
        BUNDLE_HTTPS://RUBYGEMS__PKG__GITHUB__COM/ENVATO/: "github-packages-integration:${read-secret "rubygems.key"}"
        BUNDLE_GEMS__CONTRIBSYS__COM: "2ad4b3e5:${read-secret "contribsys-sidekiq.key"}"
        BUNDLE_BUILD__EXIV2: "--with-exiv2-include=${pkgs.exiv2.dev}/include --with-exiv2-lib=${pkgs.exiv2.lib}/lib"
        BUNDLE_BUILD__MYSQL2: "--with-mysql-dir=${pkgs.mysql80}"
        BUNDLE_BUILD__PG: "--with-pg-config=${pkgs.postgresql}/bin/pg_config"
        BUNDLE_BUILD__MAXMIND_GEOIP2: "--with-maxminddb-dir=${pkgs.libmaxminddb}"
      '';
    };
    awssso = {
      target = ".aws-sso/config.yaml";
      text = ''
        SSOConfig:
          Default:
            SSORegion: us-east-1
            StartUrl: https://envatoaws.awsapps.com/start
            DefaultRegion: us-east-1
            Accounts:
              "718951516187":
                DefaultRegion: eu-west-1
        DefaultRegion: us-east-1
        ConsoleDuration: 60
        CacheRefresh: 168
        UrlAction: open
        ConfigProfilesUrlAction: open
        LogLevel: warn
        HistoryLimit: 10
        HistoryMinutes: 1440
        ProfileFormat: '{{ .AccountAlias | kebabcase }}-{{ .RoleName | kebabcase }}'
        SecureStore: keychain
        ConfigProfilesBinaryPath: ${pkgs-unstable.aws-sso-cli}/bin/aws-sso
      '';
    };
  };
}
