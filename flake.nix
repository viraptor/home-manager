{
  description = "A Home Manager flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-23.05-darwin";
    nixpkgs-2211.url = "github:nixos/nixpkgs/nixpkgs-22.11-darwin";
    nixpkgs-2205.url = "github:nixos/nixpkgs/nixpkgs-22.05-darwin";
    flake-utils.url = "github:numtide/flake-utils";
    home-manager.url = "github:nix-community/home-manager/release-23.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    bundix = {
      url = "github:nix-community/bundix?rev=4d08252ee7ed9e4b3e95b5365a1271d6df7dc8f3";
      flake = false;
    };
    #devenv.url = "github:cachix/devenv/v0.5";
    #devenv.inputs.nixpkgs.follows = "nixpkgs";
    #my-flakes.url = "path:/Users/viraptor/src/flakes";
    #my-flakes.inputs.nixpkgs.follows = "nixpkgs";
    #my-flakes.inputs.flake-utils.follows = "flake-utils";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = inputs: {
    homeConfigurations = {
      "viraptor@stan.pitucha" = let
        system = "aarch64-darwin";
        pkgs = inputs.nixpkgs.legacyPackages.${system};
      in inputs.home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        extraSpecialArgs = {
          inherit (inputs) bundix nixpkgs-unstable nixpkgs-2211 nixpkgs-2205;
        };

        modules = [
          ./home.nix
        ];
      };
    };
  };
}
